package data;
import java.util.Iterator;

/**
* <p>Title: ContinuousAttribute</p>
* <p>Description: Classe che estende la classe Attribute.</p>
* <p>Copyright: Copyright (c) 2014</p>
* <p>Company: Metodi Avanzati di Programmazione Group.</p>
* <p>Class description: Modella un attributo continuo (numerico) rappresentandone il dominio [min,max].</p>
* @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Pignatelli
* @version 1.0
*/
public class ContinuousAttribute extends Attribute implements Iterable<Float>{
	
	//MEMBRI ATTRIBUTI
	
	private float max;
	private float min; //rappresentano gli estremi di un intervallo
	
	//MEMBRI METODI
	
	/**
	 * Invoca il costruttore della superclasse e avvalora gli estremi dell'intervallo.
	 * @param name attributo della superclasse Attribute, contenente il nome simbolico dell'attributo.
	 * @param index attributo della superclasse Attribute, contenente l'identificativo numerico dell'attributo.
	 * @param {@link #min} valore numerico dell'estremo inferiore dell'intervallo.
	 * @param {@link #max} valore numerico dell'estremo superiore dell'intervallo.
	 * @see Attribute#Attribute(String, int)
	 */
	ContinuousAttribute(String name, int index, float min, float max)
	{

		super(name, index);
		this.max = max;
		this.min = min;
	}
	
	/**
	 * Restituisce il valore numerico del membro min.
	 * @return {@link #min} valore numerico dell'estremo inferiore.
	 */
	float getMin()
	{

		return min;
	}
	
	/**
	 * Restituisce il valore numerico del membro max.
	 * @return {@link #max} valore numerico dell'estremo superiore.
	 */
	float getMax()
	{

		return max;
	}
	
	/**
	 * Istanzia e restituisce un riferimento ad oggetto di classe ContinuousAttributeIterator con numero di intervalli
	 * di  discretizzazione pari a 5.
	 * @return riferimento a una istanza della classe ContinuousAttributeIterator con paramentri getMin(), getMax() e 5 (numero di
	 * 		   intervalli di discretizzazione).
	 * @see #getMax()
	 * @see #getMin()
	 */
	public Iterator<Float> iterator()
	{
		return new ContinuousAttributeIterator(getMin(), getMax(), 5);
	}

}
