package data;

import java.io.Serializable;
/**
* <p>Title: Attribute</p>
* <p>Description: Classe astratta che modella un generico attributo. </p>
* <p>Copyright: Copyright (c) 2014</p>
* <p>Company: Metodi Avanzati di Programmazione Group</p>
* <p>Class description: Modella un generico attributo discreto o continuo e relativo identificativo numerico.</p>
* @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Pignatelli
* @version 1.0
*/
public abstract class Attribute implements Serializable{
	
	//MEMBRI ATTRIBUTI
	
	protected String name; //nome simbolico dell'attributo
	protected int index; //identificativo numerico dell'attributo
	
    //MEMBRI METODI
	
	/**
	 * Inizializza i valori dei membri {@link #name} e {@link #index}.
	 * @param name nome simbolico dell'attributo.
	 * @param index identificativo numerico dell'attributo.
	 */
	public Attribute(String name, int index)
	{	
		this.name = name;
		this.index = index;
	}
	
	/**
	 * Restituisce il valore del membro name.
	 * @return name simbolico dell'attributo.
	 */
	String getName()
	{
		return name;
	}
	
	/**
	 * Restituisce il valore numerico {@link #index}.
	 * @return {@link #index} identificativo numerico dell'attributo.
	 */
	public int getIndex()
	{		
		return index;
	}
	
	/**
	 * Restituisce il valore dell'attributo name sottoforma di stringa, chiamando il metodo {@link #getName()}.
	 * @return valore dell'attributo name sottoforma di stringa, chiamando il metodo {@link #getName()}.
	 * @see #getName()
	 */
	public String toString()
	{
		return getName();
	}
}
