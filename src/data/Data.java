package data;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.LinkedList;

import database.NoValueException;
import database.QUERY_TYPE;
import database.DatabaseConnectionException;
import database.DbAccess;
import database.TableData.TupleData;
import database.TableSchema;
import database.TableData;
/**
* <p>Title: Data</p>
* <p>Description: Classe che modella un insieme di transazioni.</p>
* <p>Copyright: Copyright (c) 2014</p>
* <p>Company: Metodi Avanzati di Programmazione Group</p>
* <p>Class description: Classe che modella un insieme di transazioni (vettori attributo-valore).</p>
* @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Pignatelli
* @version 1.0
*/
public class Data {
	
	//MEMBRI ATTRIBUTI
	
	/**
	 * @uml.property  name="data" multiplicity="(0 -1)" dimension="2"
	 */
	private Object data [][]; /* una matrice di Object che ha numero di righe pari al numero 
                                 di transazioni da memorizzare e numero di colonne pari al numero di 
                                 attributi in ciascuna transazione.*/
	/**
	 * @uml.property  name="numberOfExamples"
	 */
	private int numberOfExamples; // cardinalit� dell'insieme di transazioni.
	/**
	 * @uml.property  name="attributeSet"
	 * @uml.associationEnd  multiplicity="(0 -1)"
	 */
	private List<Attribute> attributeSet; // un array di attributi, che sono avvalorati in ciascuna transazione.
	
	//MEMBRI METODI
	
	/**
	 * Costruttore che riceve in input una stringa (tableName), popola la matrice di oggetti {@link #data} e la lista tipizzata con
	 * Attribute {@link #attributeSet}. Avvia inoltre la connessione al database e la chiude alla fine.
	 * @param tableName rappresenta la tabella dalla quale leggere i pattern nel database.
	 */
	public Data(String tableName){
		
		//data
		
		try {
			DbAccess.initConnection();
			TableSchema tSchema = new TableSchema(tableName);
			
			try {
				Statement statement =  DbAccess.getConnection().createStatement();
				ResultSet resSet = statement.executeQuery("select count(*) from " + tableName);
				resSet.next();
				int numRows = resSet.getInt(1);
				numberOfExamples = numRows;
				resSet.close();
				statement.close();
				
				data = new Object [numRows][tSchema.getNumberOfAttributes()];
				TableData tableData = new TableData();
				List<TupleData> transactionList = null;
				try {
					transactionList = tableData.getTransazioni(tableName);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
				int j = 0;

				for(TupleData o : transactionList)
				{
					int i = 0;
					
					for(Object obj : o.tuple)
					{
						data[j][i] = obj;
						i++;
					}
					
					j++;
				}
				
				
				//explanatory Set
				
				attributeSet = new LinkedList<Attribute>();
				
				for(int i=0; i<tSchema.getNumberOfAttributes(); i++)
				{
					try {
						List<Object> distinctVal = tableData.getDistinctColumnValues(tableName, tSchema.getColumn(i));
						String atValues[] = new String[distinctVal.size()];
						
						if(tSchema.getColumn(i).isNumber())
							attributeSet.add(new ContinuousAttribute(tSchema.getColumn(i).getColumnName(), i, (Float) tableData.getAggregateColumnValue(tableName, tSchema.getColumn(i), QUERY_TYPE.MIN), (Float) tableData.getAggregateColumnValue(tableName, tSchema.getColumn(i), QUERY_TYPE.MAX)));
						else
						{
							int k = 0;
							
							
							for(Object o: distinctVal)
							{
								atValues[k] = (String) o;
								k++;
							}
							
							attributeSet.add(new DiscreteAttribute(tSchema.getColumn(i).getColumnName(), i, atValues));
						}
						
					} catch (SQLException e) {
						e.printStackTrace();
					} catch (NoValueException e) {
						e.printStackTrace();
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}	
			
		} catch (DatabaseConnectionException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally
		{
			DbAccess.closeConnection();
		}

	}
	
	/**
	 * Restituisce il valore del membro {@link #numberOfExamples}, rappresentante la cardinalit� dell'insieme di transizioni
	 * @return {@link #numberOfExamples} rappresentazione numerica della cardinalit� dell'insieme di transizioni.
	 */
	public int getNumberOfExamples(){

		return numberOfExamples;
	}
	
	/**
	 * Restituisce la cardinalit� del membro {@link #attributeSet}, rappresentante l'insieme degli attributi.
	 * @return Valore numerico della cardinalit� dell'insieme degli attributi contenuto in attributeSet, mediante il metodo
	 * 		   {@link List#size()} della classe List.
	 * @see List#size()
	 */
	public int getNumberOfAttributes(){

		return attributeSet.size();
	}
	
	/**
	 * Restituisce il valore memorizzato in {@link #data} dell' attributo attributeIndex per la transazione exampleIndex.
	 * @param exampleIndex valore della riga della matrice {@link #data}, indicante una specifica transazione.
	 * @param attributeIndex valore della colonna della matrice {@link #data}, indicante uno specifico attributo.
	 * @return Valore assunto dall�attributo nella matrice {@link #data} identificato da attributeIndex nella transazione
	 *  	   exampleIndex.
	 */
	public Object getAttributeValue(int exampleIndex, int attributeIndex){
			
		return data[exampleIndex][(attributeSet.get(attributeIndex)).getIndex()];
	}
	
	/**
	 * Restituisce l'attributo in posizione index di attributeSet.
	 * @param index indice numerico di un attributo.
	 * @return Valore corrispondente all'indice index della lista {@link #attributeSet}, mediante l'uso del metodo {@link List#get(int)} della
	 * 		   classe List.
	 * @see List#get(int)
	 */
	public Attribute getAttribute(int index){
		return attributeSet.get(index);
	}
	
	/**
	 *Per ogni transazione memorizzata in data, concatena in una stringa i valori assunti dagli 
     *attributi nella transazione separati da virgole. Le stringhe che rappresentano 
     *ogni transazione sono poi concatenate in un'unica stringa da restituire in output. 
     *@return value stringa con i valori assunti dagli attributi concatenati e separati da virgole.
     *@see Object#toString()
	 */
	public String toString(){

		String value="";
		for(int i=0;i<numberOfExamples;i++){
			value+=(i+1)+":";
			for(int j=0;j<attributeSet.size()-1;j++)
				value+=data[i][j]+",";
			
			value+=data[i][attributeSet.size()-1]+"\n";
		}
		return value;
		
		
	}

	/**
	 * Verifica se la cardinalit� dell'insieme delle transazioni � vuoto, restituisce un valore booleano true se vero, false
	 * altrimenti.
	 * @return Valore booleano true se la cardinalit� dell'insieme delle transazioni � pari a 0, false altrimenti.
	 */
	public boolean datasetEmpty()
	{
		return numberOfExamples == 0;
	}
	


}
