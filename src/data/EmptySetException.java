package data;
/**
* <p>Title: EmptySetException</p>
* <p>Description: Classe che modella un'eccezione </p>
* <p>Copyright: Copyright (c) 2014</p>
* <p>Company: Metodi Avanzati di Programmazione Group</p>
* <p>Class description: Modella l'eccezione che occorre qualora l'insieme di training fosse vuoto (non contiene transazioni/esempi). Tale eccezione � sollevata nei costruttori di FrequentPatternMiner e EmergingPatternMiner.</p>
* @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Pignatelli
* @version 1.0
*/
public class EmptySetException extends Exception {
	
	public EmptySetException() {}
	/**
	 * Modella l'eccezione che occorre qualora l'insieme di training fosse vuoto. Estende la classe Exception.
	 * @param msg stringa contenente il messaggio di errore.
	 */
	public EmptySetException(String msg)
	{
		super(msg);
	}

}
