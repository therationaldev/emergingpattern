package data;

import java.util.Iterator;
/**
* <p>Title: ContinuousAttributeIterator</p>
* <p>Description: Classe che che implementa l�interfaccia Iterator&#60;Float&#62;. </p>
* <p>Copyright: Copyright (c) 2014</p>
* <p>Company: Metodi Avanzati di Programmazione Group</p>
* <p>Class description: Realizza l�iteratore che itera sugli elementi della sequenza composta da numValues valori reali equidistanti tra di loro (cut points)
* 						compresi tra min e max ottenuti per mezzo di discretizzazione. La classe implementa i metodi della
*  						interfaccia generica Iterator&#60;T&#62; tipizzata con Float.</p>
* @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Pignatelli
* @version 1.0
*/
public class ContinuousAttributeIterator implements Iterator<Float> {

	private float min;
	private float max;
	private int j=0;
	private int numValues;
	
	/**
	 * Avvalora i membri attributo della classe con i parametri del costruttore.
	 * @param min valore minimo dell'intervallo.
	 * @param max valore massimo dell'intervallo.
	 * @param numValues numero di intervalli di discretizzazione.
	 */
	ContinuousAttributeIterator(float min,float max,int numValues){
		this.min=min;
		this.max=max;
		this.numValues=numValues;
	}
	
	/**
	 * Restituisce true se ci sono altri valori nell'intervallo di descretizzazione, false altrimenti.
	 * @return valore booleano true se la posizione dell'iteratore (j) della collezione di cut point � minore del numero di
	 * 		   intervalli di discretizzazione, false altrimenti.
	 */
	public boolean hasNext() {
		return (j<=numValues);
	}

	/**
	 * Incrementa j, restituisce il cut point in posizione j-1*({@link #min} + (j-1)*({@link #max}-{@link #min})/{@link #numValues}). 
	 * @return		valore intero raffigurante il cut point in posizione j-1*({@link #min} + (j-1)*({@link #max}-{@link #min})/{@link #numValues}).
	 */
	public Float next() {
		j++;
		return min+((max-min)/numValues)*(j-1);
	}

	public void remove() {

	}

}
