package database;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
/**
* <p>Title: TableSchema</p>
* <p>Description: Modella lo schema di una tabella nel database relazionale.</p>
* <p>Copyright: Copyright (c) 2014</p>
* <p>Class description: Modella lo schema di una tabella nel database relazionale.</p>
* @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Pignatelli
* @version 1.0
*/
public class TableSchema {
	/**
	 * Gestione di un attributo/colonna di una generica relazione.
	 */
	public class Column{
		private String name;
		private String type;
		/**
		 *Inizializza gli attributi {@link #name} e {@link #type} con i parametri passati.
		 * @param name nome dell'attributo.
		 * @param type tipo dell'attributo.
		 */
		Column(String name,String type){
			this.name=name;
			this.type=type;
		}
		/**
		 * Restituisce il nome dell'attributo.
		 * @return name nome dell'attributo.
		 */
		public String getColumnName(){
			return name;
		}
		/**
		 * Verifica se l'attributo � di tipo numerico o no.
		 * @return true se l'attributo � numerico, false altrimenti.
		 */
		public boolean isNumber(){
			return type.equals("number");
		}
		/**
		 * Restituisce una stringa con nome e tipo dell'attributo.
		 * @return stringa nella forma "nome : tipo".
		 */
		public String toString(){
			return name+":"+type;
		}
	}
	List<Column> tableSchema=new ArrayList<Column>();
	
	/**
	 * Estrapola lo schema della relazione il cui nome � passato come parametro.
	 * @param tableName nome della relazione.
	 * @throws SQLException
	 */
	public TableSchema(String tableName) throws SQLException{
		HashMap<String,String> mapSQL_JAVATypes=new HashMap<String, String>();
		//http://java.sun.com/j2se/1.3/docs/guide/jdbc/getstart/mapping.html
		mapSQL_JAVATypes.put("CHAR","string");
		mapSQL_JAVATypes.put("VARCHAR","string");
		mapSQL_JAVATypes.put("LONGVARCHAR","string");
		mapSQL_JAVATypes.put("BIT","string");
		mapSQL_JAVATypes.put("SHORT","number");
		mapSQL_JAVATypes.put("INT","number");
		mapSQL_JAVATypes.put("LONG","number");
		mapSQL_JAVATypes.put("FLOAT","number");
		mapSQL_JAVATypes.put("DOUBLE","number");
		
		
	
		 Connection con=DbAccess.getConnection();
		 DatabaseMetaData meta = con.getMetaData();
	     ResultSet res = meta.getColumns(null, null, tableName, null);
		   
	     while (res.next()) {
	         
	         if(mapSQL_JAVATypes.containsKey(res.getString("TYPE_NAME")))
	        		 tableSchema.add(new Column(
	        				 res.getString("COLUMN_NAME"),
	        				 mapSQL_JAVATypes.get(res.getString("TYPE_NAME")))
	        				 );
	
	         
	         
	      }
	      res.close();
	    
	    }
	  
	/**
	 * Restituisce il numero di attributi della relazione.
	 * @return valore numerico indicante il numero di attributi.
	 */
		public int getNumberOfAttributes(){
			return tableSchema.size();
		}
		/**
		 * Restituisce un riferimento alla colonna identificata con il numero passato come parametro.
		 * @param index indice dell'attributo richiesto.
		 * @return restituisce la colonna associata all'indice index preso in input.
		 */
		public Column getColumn(int index){
			return tableSchema.get(index);
		}

		
	}

		     


