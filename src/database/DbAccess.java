package database;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;
/**
 * <p>Title: DbAccess</p>
 * <p>Description: Classe che gestisce l'accesso al database</p>
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Class description: Classe che crea e gestisce una connessione con il database MySql.</p>
 * @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Pignatelli
 * @version 1.0
 */
public class DbAccess {
	
	/**
	 *Driver da usare per connettersi al database.
	 */
	private static final String DRIVER_CLASS_NAME = "org.gjt.mm.mysql.Driver";
	
	/**
	 * Contiene parte del database url, nello specifico il tipo di database e il sottoprotocollo.
	 */
	private static final String DBMS = "jdbc:mysql";
	
	/**
	 * Contiene l'identificativo del server su cui risiede la base di dati.
	 */
	private static final String SERVER = "localhost";
	
	/**
	 * Contiene il nome della base di dati.
	 */
	private static final String DATABASE = "classificationdata";
	
	/**
	 * La porta su cui il DMBS MySQL accetta le connessioni.
	 */
	private static final int PORT=3306;
	
	/**
	 * Contiene il nome dell'utente per l'accesso alla base di dati.
	 */
	private static final String USER_ID = "decisionTreeId";
	
	/**
	 * Contiene la password di autenticazione per l'utente identificato da USER_ID.
	 */
	private static final String PASSWORD = "decTreePassword";
	
	/**
	 * Contiene il riferimento alla connessione al database.
	 */
	private static Connection conn;
	
	/**
	 * Impartisce al Class loader l'ordine di caricare il driver MySql e inizializza la connessione riferita da {@link #conn}.
	 * @throws DatabaseConnectionException
	 */
	public static void initConnection() throws DatabaseConnectionException
	{
		try
		{
			Class.forName(DRIVER_CLASS_NAME).newInstance();
			
			try
			{
				conn = DriverManager.getConnection(DBMS+"://" + SERVER + ":" + PORT + "/" + DATABASE, USER_ID, PASSWORD);
			}
			catch(SQLException e)
			{
				throw new DatabaseConnectionException("Errore: impossibile stabilire una connessione");
			}
			
		}
		catch(ClassNotFoundException notFound)
		{
			throw new DatabaseConnectionException("Errore: impossibile trovare il driver");
		}
		catch(IllegalAccessException illegalAccess)
		{
			throw new DatabaseConnectionException("Errore: accesso negato su " +DRIVER_CLASS_NAME);
		}
		catch(InstantiationException instantiationException)
		{
			throw new DatabaseConnectionException("Errore: impossibile instanziare il driver");
		}
	}
	
	/**
	 * Restituisce la connessione con il database MySql ({@link #conn}).
	 * @return conn gestisce la connessione al database.
	 */
	public static Connection getConnection()
	{
		return conn;
	}
	
	/**
	 * Chiude la connessione con il database MySql.
	 */
	public static void closeConnection()
	{
		try
		{
			conn.close();
		}
		catch(SQLException e)
		{
			System.out.println("Errore nella chiusura della connessione: " +e.getMessage());
		}
	}
}
