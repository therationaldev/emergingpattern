package database;
/**
 * <p>Title: DatabaseConnectionException</p>
 * <p>Description: Classe che estende Exception e modella una eccezione</p>
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Class description: Classe che modella l'eccezione che occorre quando fallisce la connessione al database</p>
 * @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Pignatelli
 * @version 1.0
 */
public class DatabaseConnectionException extends Exception {
	/**
	 *Chiama il costuttore della superclasse e modella l'eccezione che occorre qualora fallisce la connessione al database.
	 *@param msg stringa che contiene il messaggio dell'eccezione.
	 */
	DatabaseConnectionException(String msg)
	{
		super(msg);
	}
	
}
