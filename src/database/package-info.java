/**
 * Package che contiene le classi adibite alla gestione della connessione del server al database SQL; effettua delle query al database
 * e ne restituisce i risultati.
 */
package database;