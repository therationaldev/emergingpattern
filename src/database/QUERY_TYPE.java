package database;
/**
* <p>Title: QUERY_TYPE</p>
* <p>Description: Classe enumerativa.</p>
* <p>Copyright: Copyright (c) 2014</p>
* <p>Class description: Classe enumerativa con valori MIN e MAX.</p>
* @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Pignatelli
* @version 1.0
*/
public enum QUERY_TYPE {
	MIN, MAX
}
