package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import database.TableSchema.Column;
/**
* <p>Title: TableData</p>
* <p>Description: Classe che modella le tuple di una tabella.</p>
* <p>Copyright: Copyright (c) 2014</p>
* <p>Class description: Classe che modella l'insieme di tuple collezionate in una tabella. La singola tupla � modellata
*                       dalla innerclass TupleData.</p>
* @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Pignatelli
* @version 1.0
*/
public class TableData {

	
	public class TupleData{
		public List<Object> tuple=new ArrayList<Object>();
		/**
		 * Restituisce una stringa contenente le tuple aggregate separate da uno spazio.
		 * @return value stringa contenente le tuple aggregate.
		 */
		public String toString(){
			String value="";
			Iterator<Object> it= tuple.iterator();
			while(it.hasNext())
				value+= (it.next().toString() +" ");
			
			return value;
		}
	}

	public TableData() {
	
	}
	/**
	 * Effettua una interrogazione SQL e ricava lo schema della tabella con nome table passato come parametro.
	 * @param table rappresenta il nome tabella.
	 * @return transSet lista contenente tutta la transSet.
	 * @throws SQLException
	 */
	public List<TupleData> getTransazioni(String table) throws SQLException{
		LinkedList<TupleData> transSet = new LinkedList<TupleData>();
		Statement statement;
		TableSchema tSchema=new TableSchema(table);
		
		
		String query="select ";
		
		for(int i=0;i<tSchema.getNumberOfAttributes();i++){
			Column c=tSchema.getColumn(i);
			if(i>0)
				query+=",";
			query += c.getColumnName();
		}
		if(tSchema.getNumberOfAttributes()==0)
			throw new SQLException();
		query += (" FROM "+table);
		
		statement = DbAccess.getConnection().createStatement();
		ResultSet rs = statement.executeQuery(query);
		while (rs.next()) {
			TupleData currentTuple=new TupleData();
			for(int i=0;i<tSchema.getNumberOfAttributes();i++)
				if(tSchema.getColumn(i).isNumber())
					currentTuple.tuple.add(rs.getFloat(i+1));
				else
					currentTuple.tuple.add(rs.getString(i+1));
			transSet.add(currentTuple);
		}
		rs.close();
		statement.close();

		
		
		return transSet;

	}

	/**
	 * Formula ed esegue una interrogazione SQL per estrarre i valori distinti ordinati di column e popolare una lista da restituire.
	 * @param table nome della tabella.
	 * @param column nome della colonna della tabella.
	 * @return distinctValues lista di tuple contentente l'intera transSet.
	 * @throws SQLException
	 */
	public List<Object> getDistinctColumnValues(String table,Column column) throws SQLException{
		
		LinkedList<Object> distinctValues = new LinkedList<Object>();
		Statement statement = null;
		ResultSet rs = null;
		String query = "select distinct " + column.getColumnName() + " from " + table + " order by " + column.getColumnName() + " asc";
		
		try
		{
			statement = DbAccess.getConnection().createStatement();
			rs = statement.executeQuery(query);
			
			while(rs.next())
			{
				if(column.isNumber())
				{
					distinctValues.add(rs.getFloat(column.getColumnName()));
				}
				else
					distinctValues.add(rs.getString(column.getColumnName()));
			}
		}
		catch(SQLException e)
		{
			throw e;
		}
		finally
		{
			rs.close();
			statement.close();
		}
		
		return distinctValues;

	}
	/**
	 * Formula ed esegue una interrogazione SQL per estrarre il valore aggregato (valore minimo o valore massimo) cercato
	 * nella colonna di nome column della tabella di nome table. Il metodo solleva e propaga una NoValueException se il
	 * resultset � vuoto o il valore calcolato � pari a null.
	 * @param table nome della tabella da gestire.
	 * @param column nome della colonna.
	 * @param aggregate operatore SQL di aggregazione (MIN,MAX).
	 * @return retString se il valore aggregato � una stringa.
	 * @return retFloat se il valore aggregato � un numero.
	 * @throws SQLException
	 * @throws NoValueException
	 */
	public  Object getAggregateColumnValue(String table,Column column,QUERY_TYPE aggregate) throws SQLException,NoValueException{
		

		try
		{
			Statement statement = DbAccess.getConnection().createStatement();
			String query;
			
			switch(aggregate)
			{
				case MIN:
					query = "select min(" + column.getColumnName() + ") from " + table;
					break;
					
					
				case MAX:
					query= "select max(" + column.getColumnName() + ") from " + table;
					break;
					
					default:
						throw new NoValueException("Errore: valore calcolato null");
						
			}
			
			ResultSet rs = statement.executeQuery(query);
			if(rs == null)
				throw new NoValueException("Errore: resultset nullo");
			else if(column.isNumber())
			{
				rs.next();
				Float retFloat = (Float) rs.getFloat(1);
				rs.close();
				statement.close();
						
				if(retFloat  == null)
					throw  new NoValueException("Errore: valore calcolato null");
				
				return retFloat;
					
			}
			else
			{
				rs.next();
				String retString = (String) rs.getString(1);
				rs.close();
				statement.close();
				
				if(retString == null)
					throw new NoValueException("Errore: valore calcolato null");
				
				return retString;
			}
		}
		catch(SQLException e)
		{
			throw e;
		}			
		
		
	}

}
