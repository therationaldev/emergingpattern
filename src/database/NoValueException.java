package database;
/**
* <p>Title: NoValueException</p>
* <p>Description: Classe che estende Exception per modellare un'eccezione.</p>
* <p>Copyright: Copyright (c) 2014</p>
* <p>Class description: Classe che modella l'eccezione che occorre in assenza di un valore all�interno di un resultset.</p>
* @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Pignatelli
* @version 1.0
*/
public class NoValueException extends Exception {
	/**
	 * Chiama il costruttore della superclasse e modella l�eccezione che occorre qualora un valore � assente all'interno di un resultset.
	 * @param msg stringa che contiene il messaggio dell'eccezione.
	 * @see Exception#Exception(String)
	 */
	NoValueException(String msg)
	{
		super(msg);
	}
}
