package mining;
import data.Data;
import data.EmptySetException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.List;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.Collections;
import java.util.Comparator;
/**
* <p>Title: EmergingPatternMiner</p>
* <p>Description: Classe che modella gli emerging pattern.</p>
* <p>Copyright: Copyright (c) 2014</p>
* <p>Company: Metodi Avanzati di Programmazione Group</p>
* <p>Class description: Classe che modella la scoperta di emerging pattern a partire dalla lista di frequent pattern.</p>
* @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Pignatelli
* @version 1.0
*/
public class EmergingPatternMiner implements Iterable<EmergingPattern>, Serializable{
	
	//MEMBRI ATTRIBUTI

	private List<EmergingPattern> epList; /*lista che contiene riferimenti a oggetti istanza della classe EmergingPattern 
	                           che definiscono il pattern*/
	
    //MEMBRI METODI
	
	/**
	 * Calcola il growrate degli emerging pattern tramite il rapporto tra il supporto del dataset target (mediante chiamata di {@link FrequentPattern#getSupport()}) e quello del dataset di background del frequent pattern (mediante chiamata di {@link FrequentPattern#computeSupport(Data)} 
	 * @param dataBackground insieme di transazioni di background (sar� passato come input al metodo {@link FrequentPattern#computeSupport(Data)}).
	 * @param fp contiene i frequent pattern.
	 * @return calcolo del growrate come il rapporto tra i supporti del frequent pattern del dataset target e del dataset di background.
	 * @see FrequentPattern#computeSupport(Data)
	 * @see FrequentPattern#getSupport()
	 */
	float computeGrowRate(Data dataBackground, FrequentPattern fp)
	{
		return (fp.getSupport()/fp.computeSupport(dataBackground));
	}
	
	/**
	 * Verifica che il growrate di fp sia maggiore di minGr (minimo growrate). In caso affermativo crea un oggetto EmergingPattern da fp e lo restituisce, altrimenti genera una eccezione.
	 * @param dataBackground insieme di transazioni di background.
	 * @param fp contiene i frequent pattern.
	 * @param minGR valore numerico rappresentante il growrate.
	 * @return emPat che pu� contenere l'emerging patter creato da fp se la condizione sul growrate e' soddisfatta, altrimenti null.
	 * @see EmergingPatternException
	 */
	EmergingPattern computeEmergingPattern(Data dataBackground ,FrequentPattern fp,float minGR) throws EmergingPatternException
	{
		EmergingPattern emPat = null;
		float effectiveGrowrate = computeGrowRate(dataBackground, fp);
		
		if(effectiveGrowrate < minGR)
		{
			throw new EmergingPatternException();
		}
		else if(effectiveGrowrate > minGR)
			emPat = new EmergingPattern(fp, effectiveGrowrate);
		
		return emPat;
	}
	
	/**
	 * Costruttore che crea una lista linkata ({@link #epList}) a partire dal frequent pattern (fpMiner) e dal minimo growrate (minGR),
	 * rappresentante l'emerging pattern, infine ordina la lista in base al grow rate. Pu� sollevare un'eccezione se l'insieme delle transazioni di background (dataBackground) � vuoto.
	 * @param dataBackground insieme di transazioni di background.
	 * @param fpMiner rappresenta i frequent pattern.
	 * @param minGR rappresenta il minimo growrate.
	 * @see data.EmptySetException
	 * @see ComparatorGrowRate
	 */
	public EmergingPatternMiner(Data dataBackground, FrequentPatternMiner fpMiner, float minGR) throws EmptySetException
	{
		epList = new LinkedList<EmergingPattern>();
		
		if(dataBackground.datasetEmpty())
		{
			throw new EmptySetException("Eccezione sollevata: insieme di training vuoto");
		}

			for(FrequentPattern o : fpMiner)
			{
				EmergingPattern emPattern = null;
				
				try
				{
					emPattern = computeEmergingPattern(dataBackground, o, minGR);
				}
				catch(EmergingPatternException empException)
				{
					/*
					 * catturiamo l'eccezione senza visualizzare ulteriori messaggi tramite System.out.prinln o
					 * printStackTrace() in modo da evitare numerosi messaggi ogni volta che un pattern non viene aggiunto
					 */
				}
				
				if(emPattern != null)
				{
					epList.add(emPattern);
				}
			}
			
			ComparatorGrowRate comparatorEm = new ComparatorGrowRate();
			
			sort(epList, comparatorEm);
		
	}
	
	/**
	 * Scandisce {@link #epList} al fine di concatenare in un'unica stringa le stringhe rappresentanti i pattern emergenti letti.
	 * @return retStr contenente le stringhe concatenate dei pattern emergenti letti.
	 */
	public String toString()
	{
		String retStr = new String();
		
		int i = 1;
		for(EmergingPattern o : this)
		{
			retStr += Integer.toString(i) +": " + o;
			i++;
		}
		
		return retStr;
	}
	
	/**
	 * Implementa il metodo {@link Iterable#iterator()}.
	 * @return ritorna l'iteratore di {@link #epList}.
	 */
	public Iterator<EmergingPattern> iterator()
	{
		return epList.iterator();
	}
	
	/**
	 * Ordina la lista di emerging pattern l1 usando il comparatore associato alla classe EmergingPattern.
	 * @param l1 lista tipizzata con EmergingPattern contenente pattern emergenti.
	 * @param comparator comparatore che indica il metodo di ordinamento dei pattern emergenti.
	 */
	private void sort(List<EmergingPattern> l1, Comparator<EmergingPattern> comparator)
	{
		Collections.sort(l1, comparator);
	}
	/**
	 * Salva su file di nome nomeFile la lista di emerging pattern.
	 * @param nomeFile nome del file su cui salvare.
	 * @throws FileNotFoundException
	 * @throws IOException eccezione
	 */
	public void salva(String nomeFile) throws FileNotFoundException, IOException
	{
		
		try
		{
			ObjectOutputStream outStream = new ObjectOutputStream(new FileOutputStream(nomeFile));
			try
			{
				outStream.writeObject(this);
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
			outStream.close();
		}
		catch(FileNotFoundException e)
		{
			System.out.println("Eccezione: " + e.getMessage());
		}
		catch(IOException o)
		{
			System.out.println("Eccezione: " + o.getMessage());
		}
		
	}
	/**
	 * Carica da file con nome nomeFile gli emerging pattern.
	 * @param nomeFile nome del file da cui caricare gli emerging pattern.
	 * @return null.
	 * @throws FileNotFoundException
	 * @throws IOException eccezione
	 * @throws ClassNotFoundException
	 */
	public static EmergingPatternMiner carica(String nomeFile) throws FileNotFoundException, IOException, ClassNotFoundException
	{	
		try
		{
			ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(nomeFile));
			EmergingPatternMiner loadedEP = (EmergingPatternMiner) inputStream.readObject();
			inputStream.close();
			return loadedEP;
		}
		catch(FileNotFoundException e)
		{
			System.out.println("Eccezione: " + e.getMessage());
		}
		catch(IOException o)
		{
			System.out.println("Eccezione: " + o.getMessage());
		}
		catch(ClassNotFoundException u)
		{
			System.out.println("Eccezione: " + u.getMessage());
		}
		
		return null;
	}
	
	/**
	 * Restituisce la lunghezza della lista contenente gli emerging patterns.
	 * @return 	  lunghezza della lista {@link #epList}
	 */
	public int getEPMinerLength()
	{
		return epList.size();
	}
	
	/**
	 * Restituisce un array bidimensionale di float contenente ordinatamente i valori dei supports e del grow rate dei pattern emergenti individuati nella lista {@link #epList}.
	 * @return epData array di float.
	 */
	public float[][] getListSupportGrowrate()
	{
		float[][] epData = new float[getEPMinerLength()][2];
		
		int i = 0;
		for(EmergingPattern o : this)
		{
			epData[i][0] = o.getSupport();
			epData[i][1] = o.getGrowRate();
			i++;
		}
		
		return epData;
	}
}
