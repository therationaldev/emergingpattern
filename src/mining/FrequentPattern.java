package mining;
import data.Data;
import data.DiscreteAttribute;
import data.ContinuousAttribute;

import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;
import java.io.Serializable;
//import java.lang.Iterable;

/**
* <p>Title: FrequentPattern</p>
* <p>Description: Classe che modella un frequent pattern </p>
* <p>Copyright: Copyright (c) 2014</p>
* <p>Company: Metodi Avanzati di Programmazione Group</p>
* <p>Class description: Classe che rappresenta un itemset (o pattern) frequente.</p>
* @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Pignatelli
* @version 1.0
*/
 class FrequentPattern implements Iterable<Item>, Comparable<FrequentPattern>, Serializable{

	 //MEMBRI ATTRIBUTI
	/**
	 * @uml.property  name="fp"
	 * @uml.associationEnd  multiplicity="(0 -1)"
	 */
	private List<Item> fp;
	/**
	 * @uml.property  name="support"
	 */
	private float support;
	
	//MEMBRI METODI
	
	/**
	 * Inizializza {@link #fp} come una lista linkata tipizzata a Item
	 */
	FrequentPattern(){
		fp=new LinkedList<Item>();
	}
	
	// costruttore per copia
	/**
	 * Costruttore per copia che alloca {@link #fp} e {@link #support} come copia
	 * del frequent pattern FP passato.
	 * @param FP contiene i frequent pattern da copiare.
	 */
	FrequentPattern(FrequentPattern FP){

		int length =FP.getPatternLength();
		fp = new LinkedList<Item>();
		for(int i=0;i<length;i++)
			fp.add((Item) FP.getItem(i));
		
		
		support=FP.getSupport();
		
		
	}
	//aggiunge un nuovo item al pattern
	/**
	 * Si estende la dimensione di {@link #fp} di 1 e si inserisce in ultima posizione l'argomento della procedura.
	 * @param item oggetto da inserire nei frequent pattern.
	 */
	void addItem(Item item)
	{	
		fp.add(item);	
	}
	
	/**
	 * Restituisce l'item in posizione index di {@link #fp}.
	 * @param index indice dell'item all'interno di {@link #fp} da restituire.
	 * @return fp[index] elemento all'interno di {@link #fp} in posizione index.
	 */
	Item getItem(int index)
	{
		return fp.get(index);
	}
	
	/**
	 * restituisce il valore di {@link #support}.
	 * @return support valore di {@link #support}.
	 */
	float getSupport()
	{
		return support;
	}
	
	/**
	 * Assegna al membro {@link #support} il valore passato alla procedura.
	 * @param support Valore del supporto del pattern da inserire.
	 */
	void setSupport(float support)
	{
		this.support = support;
	}
	
	/**
	 * Restituisce la dimensione (lunghezza) di {@link #fp}, cio� la quantit� di item dei frequent pattern.
	 * @return Valore numerico che indica la dimensione della lista {@link #fp}.
	 */ 
	int getPatternLength()
	{
		return fp.size();
	}
	
	/**
	 * Si scandisce fp al fine di concatenare in una stringa la rappresentazione degli item,
	 * infine si concatena il supporto.
	 */
	public String toString(){
		
		Iterator<Item> e = this.iterator();
		String value="";
		
		while(e.hasNext())
		{
			value+= e.next() ;
			
			if(e.hasNext())
				value += " AND ";
		}
		if(fp.size()>0){
			value+="["+support+"]\n";
		}
		
		return value;
	}

	// Aggiorna il supporto
	
	/**
	 * Calcola il supporto del pattern rappresentato dall'oggetto this
	 * rispetto al dataset data passato come argomento. Il metodo controlla che l'item in esame sia un attributo discreto o continuo.
	 * @param data dataset dal quale calcolare il supporto del frequent pattern.
	 * @return valore numerico che indica il supporto dei frequent pattern.
	 */
	float computeSupport(Data data){
	
		int suppCount=0;
			// indice esempio
			for(int i=0;i<data.getNumberOfExamples();i++){
				//indice item
				boolean isSupporting=true;
				for(int j=0;j<this.getPatternLength();j++)
				{
					if (this.getItem(j) instanceof DiscreteItem)
					{
						//DiscreteItem
						DiscreteItem item=(DiscreteItem)this.getItem(j);
						DiscreteAttribute attribute=(DiscreteAttribute)item.getAttribute();
						
						//Extract the example value
						Object valueInExample=data.getAttributeValue(i, attribute.getIndex());
						if(!item.checkItemCondition(valueInExample)){
							isSupporting=false;
							break; //the ith example does not satisfy fp
						}
					}
					else if(this.getItem(j) instanceof ContinuousItem)
					{
						ContinuousItem item=(ContinuousItem) this.getItem(j);
						ContinuousAttribute attribute=(ContinuousAttribute)item.getAttribute();
						
						Object valueInExample = data.getAttributeValue(i, attribute.getIndex());
						if(!item.checkItemCondition(valueInExample)){
							isSupporting=false;
							break; //the ith example does not satisfy fp
						}
//						
						
					}
					
					
				}
				if(isSupporting)
					suppCount++;
			}
			return((float)suppCount)/(data.getNumberOfExamples());
			
		}
		
	
	/**
	 * Iteratore della lista tipizzata ad Item {@link #fp}.
	 * @see java.lang.Iterable#iterator()
	 */
	public Iterator<Item> iterator()
	{
		return fp.iterator();
	}
	
	/**
	 * Implementa il metodo {@link Comparable#compareTo(Object)} dell'interfaccia Comparable
	 * effettuando il confronto basandosi sull'attributo {@link #support}.
	 * @param obj frequent pattern da ordinare in base al support
	 * @return -1 se il support del parametro e' minore, 0 se e' uguale, 1 se e' maggiore
	 */
	public int compareTo(FrequentPattern obj)
	{
		if(obj.getSupport() < this.getSupport())
			return 1;
		
		else if(obj.getSupport() == this.getSupport())
			return (int) 0;
		
		else
			return -1;
	}
}
