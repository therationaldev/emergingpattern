package mining;
import data.ContinuousAttribute;
/**
* <p>Title: ContinuousItem</p>
* <p>Description: Classe che implementa l'intefaccia Comparator</p>
* <p>Copyright: Copyright (c) 2014</p>
* <p>Company: Metodi Avanzati di Programmazione Group</p>
* <p>Class description: Estende la classe astratta Item e modella la coppia &#60;Attributo continuo - Intervallo di valori&#62;</p>
* @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Pignatelli
* @version 1.0
*/
public class ContinuousItem extends Item {

	/**
	 * Chiama il costruttore della superclasse passandogli come argomenti un attributo continuo e un intervallo di valori.
	 * @param attribute rappresentante l'attributo continuo.
	 * @param value rappresentante l'intervallo di valori.
	 * @see Item#Item(data.Attribute, Object)
	 */
	public ContinuousItem(ContinuousAttribute attribute, Interval value)
	{
		super(attribute, value);
	}
	
	/** 
	 * Verifica che il parametro value rappresenti un numero reale incluso tra gli estremi dell�intervallo associato all'item
	 * in oggetto, per far ci� richiama il metodo {@link mining.Interval#checkValueInclusion(float)}
	 * @param value valore che, in run time, sar� di tipo Float.
	 * @return valore booleano true se il parametro value � incluso nell'intervallo associato all'item in oggetto, false altrimenti.
	 * @see mining.Interval#checkValueInclusion(float)
	 */
	boolean checkItemCondition(Object value) {
		return ((Interval) getValue()).checkValueInclusion((float) value);
	}
	/**
	 * Sovrascrive il metodo {@link Item#toString()} e avvalora la stringa che rappresenta lo stato dell�oggetto (per esempio, Temperatura in [10;20.4[) e ne restituisce il riferimento.
	 * @return retStr stringa contenente il riferimento alla stringa avvalorata.
	 * @see Item#toString()
	 */
	public String toString()
	{
		String retStr = getAttribute() + " in " + ((Interval) getValue());
		return retStr;
	}
}
