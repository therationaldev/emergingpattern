package mining;
/**
* <p>Title: EmergingPatternException</p>
* <p>Description: Classe che estende la classe Exception e modella un'eccezione </p>
* <p>Copyright: Copyright (c) 2014</p>
* <p>Company: Metodi Avanzati di Programmazione Group</p>
* <p>Class description: Classe che modella l�eccezione che occorre qualora il pattern corrente non soddisfa la condizione di minimo grow rate.</p>
* @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Pignatelli
* @version 1.0
*/
public class EmergingPatternException extends Exception {
	
	public EmergingPatternException() {}
	/**
	 * Chiama il costruttore della superclasse passandogli la stringa msg. Modella l�eccezione che occorre qualora
	 * il pattern corrente non soddisfa la condizione di minimo grow rate.
	 * @param msg stringa che contiene il massaggio dell'eccezione.
	 * @see Exception#Exception(String)
	 */
	public EmergingPatternException(String msg)
	{
		super(msg);
	}

}
