package mining;
import utility.Queue;
import data.Attribute;
import data.Data;
import data.DiscreteAttribute;
import data.ContinuousAttribute;
import data.EmptySetException;
import utility.EmptyQueueException;

import java.util.List;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.Collections;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
/**
* <p>Title: FrequentPatternMiner</p>
* <p>Description: Classe che modella dei frequent pattern.</p>
* <p>Copyright: Copyright (c) 2014</p>
* <p>Company: Metodi Avanzati di Programmazione Group</p>
* <p>Class description: Classe che include i metodi per la scoperta di pattern frequenti con Algoritmo APRIORI.</p>
* @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Pignatelli
* @version 1.0
*/
public class FrequentPatternMiner implements Iterable<FrequentPattern>, Serializable{
	
	//MEMBRI ATTRIBUTI
	private 		List<FrequentPattern> outputFP=new LinkedList<FrequentPattern>();
	
	//MEMBRI METODI
	
	/**
	 * Genera tutti i pattern k=1 frequenti e per ognuno di questi genera
	 * quelli con k&#62;1 richiamando {@link #expandFrequentPatterns(Data, float, Queue, List)}. I pattern sono memorizzati
	 * nell'attributo {@link #outputFP}.
	 * @param data insieme delle transazioni.
	 * @param minSup valore che indica il supporto minimo.
	 * @throws EmptySetException
	 */
	public FrequentPatternMiner(Data data,float minSup) throws EmptySetException{
	
		Queue fpQueue=new Queue();
		
		if(data.datasetEmpty())
		{
			throw new EmptySetException("Eccezione sollevata: dataset vuoto");
		}
	
		for(int i=0;i<data.getNumberOfAttributes();i++)
		{
			Attribute currentAttribute=data.getAttribute(i);
			
			if(currentAttribute instanceof DiscreteAttribute)
				for(int j=0;j<((DiscreteAttribute)currentAttribute).getNumberOfDistinctValues();j++)
				{
					DiscreteItem item=new DiscreteItem( 
							(DiscreteAttribute)currentAttribute, 
							((DiscreteAttribute)currentAttribute).getValue(j));
					FrequentPattern fp=new FrequentPattern();
					fp.addItem(item);
					fp.setSupport(fp.computeSupport(data));
					if(fp.getSupport()>=minSup){ // 1-FP CANDIDATE
						fpQueue.enqueue(fp);
						outputFP.add(fp);
					}
					
				}
			else if(currentAttribute instanceof ContinuousAttribute)
			{
				Iterator<Float> e = ((ContinuousAttribute)currentAttribute).iterator();
				Float firstExtreme = e.next();
				
				while(e.hasNext())
				{
					Float secondExtreme = e.next();
					Interval intervalValue = new Interval(firstExtreme, secondExtreme);
					ContinuousItem item=new ContinuousItem( 
							(ContinuousAttribute)currentAttribute, 
							intervalValue);
					FrequentPattern fp=new FrequentPattern();
					fp.addItem(item);
					fp.setSupport(fp.computeSupport(data));
					if(fp.getSupport()>=minSup){ // 1-FP CANDIDATE
						fpQueue.enqueue(fp);
						outputFP.add(fp);
					}
					
					firstExtreme = secondExtreme;
					
				}
			}	
			
		}
		try
		{
			outputFP=expandFrequentPatterns(data,minSup,fpQueue,outputFP);
			sort(outputFP);
		}
		catch(EmptyQueueException queueException)
		{
			System.out.println("Si � verificato un errore -> " +queueException.getMessage());
		}
	}
	
	/**
	 * Crea un nuovo pattern a cui aggiungere tutti gli item di FP e il parametro item.
	 * @param FP frequent pattern da raffinare.
	 * @param item da aggiungere ai frequent pattern.
	 * @return newFp frequent pattern raffinato.
	 */
	FrequentPattern refineFrequentPattern(FrequentPattern FP, Item item)
	{
		FrequentPattern newFp = new FrequentPattern(FP);
		newFp.addItem(item);
		
		return newFp;
	}
	
	/**
	 * Finch� fpQueue contiene elementi, si estrae un elemento dalla coda fpQueue, si generano i raffinamenti per questo
	 * (aggiungendo un nuovo item non incluso). Per ogni raffinamento si verifica se � frequente e, in caso affermativo,
	 * lo si aggiunge sia ad fpQueue sia ad {@link #outputFP}.
	 * @param data rappresenta l�insieme delle transazioni.
	 * @param minSup rappresenta il minimo supporto.
	 * @param fpQueue rappresenta coda contenente i pattern da valutare.
	 * @param outputFP rappresenta lista dei frequent pattern da valutare.
	 * @return outputFP lista linkata popolata con pattern frequenti a k>1.
	 */
	 private   List<FrequentPattern> expandFrequentPatterns(Data data, float minSup, Queue fpQueue,List<FrequentPattern> outputFP) throws EmptyQueueException
	 {
		if(fpQueue.isEmpty())
		{
			 throw new EmptyQueueException("Eccezione sollevata: coda vuota");
		}
		 
		while (!fpQueue.isEmpty()){
			FrequentPattern fp=(FrequentPattern)fpQueue.first(); //fp to be refined
			fpQueue.dequeue();
			for(int i=0;i<data.getNumberOfAttributes();i++){
				boolean found=false;
				for(int j=0;j<fp.getPatternLength();j++) //the new item should involve an attribute different form attributes already involved into the items of fp
					if(fp.getItem(j).getAttribute().equals(data.getAttribute(i))){
						found=true;
						break;
					}
				if(!found) //data.getAttribute(i) is not involve into an item of fp
				{
					if(data.getAttribute(i) instanceof DiscreteAttribute)
						for(int j=0;j<((DiscreteAttribute)data.getAttribute(i)).getNumberOfDistinctValues();j++)
						{
							DiscreteItem item=new DiscreteItem(
									(DiscreteAttribute)data.getAttribute(i),
									((DiscreteAttribute)(data.getAttribute(i))).getValue(j)
									);
							FrequentPattern newFP=refineFrequentPattern(fp,item); //generate refinement
							newFP.setSupport(newFP.computeSupport(data));
							if(newFP.getSupport()>=minSup){
								fpQueue.enqueue(newFP);
								//System.out.println(newFP);
								outputFP.add(newFP);
							}
						}
					else if(data.getAttribute(i) instanceof ContinuousAttribute)
					{
						Iterator<Float> e = ((ContinuousAttribute)data.getAttribute(i)).iterator();
						Float firstExtreme = e.next();
						
						while(e.hasNext())
						{
							Float secondExtreme = e.next();
							Interval intervalValue = new Interval(firstExtreme, secondExtreme);
							ContinuousItem item=new ContinuousItem( 
									(ContinuousAttribute)data.getAttribute(i), 
									intervalValue);
							FrequentPattern newFP=refineFrequentPattern(fp,item); //generate refinement
							newFP.setSupport(newFP.computeSupport(data));
							if(newFP.getSupport()>=minSup){
								fpQueue.enqueue(newFP);
								outputFP.add(newFP);
							}
							
							firstExtreme = secondExtreme;
						}
					}
				}
		}
	}
		return outputFP;
	}
	 
	 /**
	  * Cicla dall'inizio dell'iteratore fino all'indice passato, quindi restituisce il pattern situato nella posizione indicata.
	  * @param position rappresenta l'indice del pattern che si vuole cercare.
	  * @return il pattern nella posizione indicata.
	  */
	FrequentPattern getPattern(int position)
	{
		Iterator<FrequentPattern> e = this.iterator();
		
		for(int i=0; i < position; i++)
			e.next();
		
		return e.next();
	}
	
	/**
	 * Scandisce OutputFP al fine di concatenare in un'unica stringa i pattern frequenti letti.
	 * @return retStr stringa che contiene i pattern frequenti letti e concatenati.
	 */
	public String toString()
	{
		String retStr = new String();
		
		int i = 1;
		for(FrequentPattern o : this)
		{
			retStr += Integer.toString(i) +": " + o;
			i++;
		}
		
		if(retStr.length() != 0)
			retStr += "\n";
		
		return retStr;
	}
	/**
	 * Restituisce l'iteratore di {@link #outputFP}.
	 * @return l'iteratore di {@link #outputFP}.
	 */
	public Iterator<FrequentPattern> iterator()
	{
		return outputFP.iterator();
	}
	
	/**
	 * Ordina la lista di frequent pattern l1 sfruttando l'interfaccia Comparable implementata da #{@link FrequentPattern}.
	 * @param l1 lista di frequent pattern da ordinare.
	 */
	private void sort(List<FrequentPattern> l1)
	{
		Collections.sort(l1);
	}
	/**
	 * Salva su file di nome nomeFile i frequent pattern.
	 * @param nomeFile nome del file su cui salvare i frequent pattern.
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void salva(String nomeFile) throws FileNotFoundException, IOException
	{
		
		try
		{
			ObjectOutputStream outStream = new ObjectOutputStream(new FileOutputStream(nomeFile));
			
			try{
				outStream.writeObject(this);
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
			outStream.close();
		}
		catch(FileNotFoundException e)
		{
			System.out.println("Eccezione: " + e.getMessage());
		}
		catch(IOException o)
		{
			System.out.println("Eccezione: " + o.getMessage());
		}
		
	}
	/**
	 * Carica i frequent pattern da file con nome nomeFile in un oggetto e lo restituisce.
	 * @param nomeFile nome del file dai quali leggere i frequent patter.
	 * @return loadedFP contenente i frequent pattern caricati.
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static FrequentPatternMiner carica(String nomeFile) throws FileNotFoundException, IOException, ClassNotFoundException
	{
		
		try
		{
			ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(nomeFile));
			FrequentPatternMiner loadedFP = (FrequentPatternMiner) inputStream.readObject();
			inputStream.close();
			
			return loadedFP;
		}
		catch(FileNotFoundException e)
		{
			System.out.println("Eccezione: " + e.getMessage());
		}
		catch(IOException o)
		{
			System.out.println("Eccezione: " + o.getMessage());
		}
		catch(ClassNotFoundException u)
		{
			System.out.println("Eccezione: " + u.getMessage());
		}
		
		return null;
	}
	
	/**
	 * Restituisce la lunghezza della lista contenente i frequent patterns.
	 * @return 		 lunghezza della lista {@link #outputFP}.
	 */
	public int getFPMinerLength()
	{
		return outputFP.size();
	}
	
	/**
	 * Restituisce un array di float contenente ordinatamente i supports dei pattern frequenti individuati nella lista {@link #outputFP}.
	 * @return fpData array di float.
	 */
	public float[] getListSupport()
	{
		float[] fpData = new float[getFPMinerLength()];
		
		int i = 0;
		for(FrequentPattern o : this)
		{
			fpData[i] = o.getSupport();
			i++;
		}
		
		return fpData;
	}
}
	


