package mining;
import java.util.Comparator;
/**
* <p>Title: ComparatorGrowRate</p>
* <p>Description: Classe che implementa l'intefaccia Comparator.</p>
* <p>Copyright: Copyright (c) 2014</p>
* <p>Company: Metodi Avanzati di Programmazione Group</p>
* <p>Class description: Classe usata per confrontare gli emerging pattern in base al growRate.</p>
* @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Pignatelli
* @version 1.0
*/
public class ComparatorGrowRate implements Comparator<EmergingPattern>{
	
	/**
	 * Implementa {@link Comparator#compare(Object, Object)} dell'interfaccia Comparator usando l'attributo growrate per i confronti
	 * ottenuto mediante il metodo {@link EmergingPattern#getGrowRate()}.
	 * @param o1 primo oggetto di confronto.
	 * @param o2 secondo oggetto di confronto.
	 * @return -1 se il growrate di o1 � minore di o2, 1 se il growrate di o1 � maggiore di o2, 0 se i due growrate sono uguali.
	 * @see Comparator#compare(Object, Object)
	 * @see EmergingPattern#getGrowRate()
	 */
	public int compare(EmergingPattern o1, EmergingPattern o2)
	{
		if( o1.getGrowRate() < o2.getGrowRate())
			return -1;
		else if( o1.getGrowRate() > o2.getGrowRate())
			return 1;
		else
			return 0;
	}
}
