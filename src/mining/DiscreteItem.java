package mining;

//MEMBRI ATTIBUTI
import data.DiscreteAttribute;

//MEMBRI METODI
/**
* <p>Title: DiscreteItem</p>
* <p>Description: Classe che estende la classe astratta Item.</p>
* <p>Copyright: Copyright (c) 2014</p>
* <p>Company: Metodi Avanzati di Programmazione Group</p>
* <p>Class description: Classe che rappresenta la coppia &#60;Attributo discreto - valore discreto&#62; (ad esempio: (Outlook="Sunny")).</p>
* @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Pignatelli
* @version 1.0
*/
class DiscreteItem extends Item {

	/**
	 * Invoca il costruttore della superclasse e avvalora i membri attribute e value.
	 * @param attribute rappresenta un attributo discreto.
	 * @param value rappresenta il valore dell'attributo discreto.
	 * @see #Item(data.Attribute, Object);
	 */
	DiscreteItem(DiscreteAttribute attribute, String value)
	{
		super(attribute, value);
	}
	
	/**
	 * Verifica che il membro value (della classe Item) sia uguale (nello stato) all'argomento passato value, mediante
	 *  il metodo {@link Object#equals(Object)}.
	 * @return valore booleano true se lo stato dell'attributo value della classe Item � uguale all'oggetto passato, false altrimenti.
	 * @see Object#equals(Object) 
	 */
	boolean checkItemCondition(Object value) {

		return this.value.equals(value);
	}
}
