package mining;
import data.Attribute;
import java.io.Serializable;
/**
* <p>Title: Item</p>
* <p>Description: Classe astratta che modella un generico idem.</p>
* <p>Copyright: Copyright (c) 2014</p>
* <p>Company: Metodi Avanzati di Programmazione Group</p>
* <p>Class description: classe che modella un generico item (coppia attributo-valore, per esempio Outlook="Sunny").</p>
* @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Pignatelli
* @version 1.0
*/
abstract class Item implements Serializable{
	
	    //MEMBRI ATTRIBUTI
		protected Attribute attribute;
		protected Object value;
		
		//MEMBRI METODI
		
		/**
		 * Inizializza i valori dei membri attributi con i parametri passati come argomento.
		 * @param attribute rappresenta un attributo coinvolto nell'item.
		 * @param value rappresenta il valore assegnato all'attributo.
		 */
		Item(Attribute attribute, Object value)
		{
			this.attribute = attribute;
			this.value = value;
		}
		
		/**
		 * restituisce il membro {@link #attribute}.
		 * @return attribute rappresenta un attributo coinvolto nell'item.
		 */
		Attribute getAttribute()
		{
			return attribute;
		}
		
		/**
		 * Restituisce il membro {@link #value}.
		 * @return value rappresenta il valore assegnato all'attributo.
		 */
		Object getValue()
		{
			return value;
		}
		
		abstract boolean checkItemCondition(Object value);
		
		/**
		 * Restituisce una stringa nella forma <attribute> = <value>.
		 * @return retStr stringa contenente gli attributi {@link #attribute} e {@link #value}.
		 */
		public String toString()
		{
			String retStr = attribute + "=" + value;
			
			return retStr;
		}
		
}
