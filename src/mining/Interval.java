package mining;

import java.io.Serializable;
/**
* <p>Title: Interval</p>
* <p>Description: Classe che modella un intervallo.</p>
* <p>Copyright: Copyright (c) 2014</p>
* <p>Company: Metodi Avanzati di Programmazione Group</p>
* <p>Class description: Classe che modella un intervallo reale [inf, sup[ l'inclusione in esso di un valore numerico.</p>
* @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Pignatelli
* @version 1.0
*/
public class Interval implements Serializable{

	private float inf; //estremo inferiore
	private float sup; //estremo superiore
	
	/**
	 * Avvalora i due attributi {@link #inf} e {@link #sup} con i parametri del costruttore.
	 * @param inf estremo inferiore dell'intervallo.
	 * @param sup estremo superiore dell'intervallo.
	 */
	Interval(float inf, float sup)
	{
		setInf(inf);
		setSup(sup);
	}
	
	/**
	 * Comportameto: avvalora inf con il parametro passato.
	 * @param inf estremo inferiore dell'intervallo.
	 */
	void setInf(float inf)
	{
		this.inf = inf;
	}
	
	/**
	 *Avvalora {@link #sup} con il parametro passato.
	 * @param sup estremo superiore dell'intervallo.
	 */
	void setSup(float sup)
	{
		this.sup = sup;
	}
	
	/**
	 *Restituisce l'estremo inferiore dell'intervallo.
	 * @return inf estremo inferiore dell'intervallo.
	 */
	float getInf()
	{
		return inf;
	}
	
	/**
	 *Restituisce l'estremo superiore dell'intervallo
	 * @return sup estremo superiore dell'intervallo
	 */
	float getSup()
	{
		return sup;
	}
	
	/**
	 * Verifica se il parametro passato (value) � incluso nell'intervallo.
	 * @param value valore assunto dall'attributo continuo per il quale verificare l'appartenenza all'intervallo.
	 * @return restituisce true se il parametro passato (value) � incluso nell'intervallo, false altrimenti.
	 */
	boolean checkValueInclusion(float value)
	{
		return (value >= inf && value < (sup + 0.001)) ? true : false; 
	}
	
	/**
	 * Rappresenta in una stringa gli estremi dell'intervallo e restituisce tale stringa.
	 * @return retStr stringa che rappresenta l'intervallo nella forma: [inf, sup[.
	 */
	public String toString()
	{
		Float Inf = inf;
		Float Sup = sup;
		String retStr;
		
		retStr = "[" + Inf.toString() + ", " + Sup.toString() + "[";
		
		return retStr;
	}
}
