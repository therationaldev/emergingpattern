package mining;
/**
* <p>Title: EmergingPattern</p>
* <p>Description: Classe che estende FrequentPattern.</p>
* <p>Copyright: Copyright (c) 2014</p>
* <p>Company: Metodi Avanzati di Programmazione Group</p>
* <p>Class description: Classe che modella la scoperta di emerging pattern a partire da un frequent pattern.</p>
* @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Pignatelli
* @version 1.0
*/
class EmergingPattern extends FrequentPattern {
	
	//MEMBRI ATTRIBUTI
	private float growrate;
	
	//MEMBRI METODI
	/**
	 * Chiama il costruttore della superclasse passandogli fp e inizializza il membro growrate con l'argomento del costruttore, inoltre inizializza l'attributo growrate con quello passato.
	 * @param fp oggetto contenente i frequent pattern.
	 * @param growrate rappresenta il growrate del pattern.
	 */
	EmergingPattern(FrequentPattern fp, float growrate)
	{
		super(fp);
		this.growrate = growrate;
	}
	
	/**
	 * Restituisce il valore del membro growrate.
	 * @return growrate rappresenta il valore numerico del growrate.
	 */
	float getGrowRate()
	{
		return growrate;
	}
	
	/**
	 * Crea e restituisce la stringa (inizializzata tramite {@link FrequentPattern#toString()}) che rappresenta il pattern, il suo supporto e il suo growrate, adoperando il metodo {@link FrequentPattern#toString()}
	 * @return retStr rappresentante la stringa concatenata nella forma: pattern [supporto] [growrate].
	 * @see FrequentPattern#toString()
	 */
	public String toString()
	{
		String retStr = super.toString();
		retStr = retStr.replace("\n", "\0");
		retStr += "[" + getGrowRate() + "]" + "\n";
		
		return retStr;
		
	}
	
}
