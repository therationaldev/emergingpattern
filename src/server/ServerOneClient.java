package server;


import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.io.IOException;

import mining.EmergingPatternMiner;
import mining.FrequentPatternMiner;
import data.Data;
import data.EmptySetException;
/**
* <p>Title: MultiServer</p>
* <p>Description: Classe adibita alla comunicazione con un singolo Client.</p>
* <p>Copyright: Copyright (c) 2014</p>
* <p>Company: Metodi Avanzati di Programmazione Group</p>
* <p>Class description: Classe che estende la classe Thread che modella la comunicazione con un unico Client.</p>
* @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Pignatelli
* @version 1.0
*/
public class ServerOneClient extends Thread {
	
	private Socket socket;
	private ObjectInputStream in;
	private ObjectOutputStream out;
	/**
	 * Inizia il membro this{@link #socket} con il parametro in input. Inizializza {@link #in} e {@link #out}, avvia il thread
	 * invocando il metodo {@link Thread#start()}.
	 * @param socket Terminale lato server del canale tramite cui avviene lo scambio di oggetti client-server.
	 * @throws IOException
	 */
	ServerOneClient(Socket socket) throws IOException
	{
		this.socket = socket;
		in = new ObjectInputStream(socket.getInputStream());
		out = new ObjectOutputStream(socket.getOutputStream());
		start();
	}
	/**
	 * Ridefinisce il metodo run della classe Thread (variazione funzionale). Gestisce le richieste del
	 * client (apprendere pattern/regole e popolare con queste archive; salvare archive in un file, avvalorare archive
	 * con oggetto serializzato nel file).
	 */
	public void run()
	{
		int opzione = 0;
		float minsup= 0.0f;
		float minGr = 0.0f;
		String targetName = null;
		String backgroundName = null;
		String nameFile = null;
		
		try
		{
			while(true)
			{
				opzione = (int) in.readObject();
				minsup = (float) in.readObject();
				minGr = (float) in.readObject();
				targetName = (String) in.readObject();
				backgroundName = (String) in.readObject();
				nameFile = (String) in.readObject();
				
				FrequentPatternMiner fpMiner = null;
				EmergingPatternMiner epMiner = null;
				
				try
				{
					
					if(opzione==1)	
					{
							
							Data dataTarget= new Data(targetName);
							Data dataBackground= new Data(backgroundName);
							
							fpMiner=new FrequentPatternMiner(dataTarget, minsup);	
							fpMiner.salva("FP_"+nameFile+"_minSup"+minsup+".dat");
							
					
							epMiner=new EmergingPatternMiner(dataBackground, fpMiner, minGr);
							epMiner.salva("EP_"+nameFile+"_minSup"+minsup+"_minGr"+minGr+".dat");
							
					}
					else
					{
						fpMiner=FrequentPatternMiner.carica("FP_"+nameFile+"_minSup"+minsup+".dat");
						epMiner=EmergingPatternMiner.carica("EP_"+nameFile+"_minSup"+minsup+"_minGr"+minGr+".dat");
					}
				}
				catch(EmptySetException e)
				{
					e.printStackTrace();
				}
				
				try
				{
					out.writeObject(fpMiner.toString());
					out.writeObject(epMiner.toString());
					
					float[] fpMinerData = fpMiner.getListSupport();
					float[][] epMinerData = epMiner.getListSupportGrowrate();
					
					out.writeObject(fpMinerData);
					out.writeObject(epMinerData);
				}
				catch(NullPointerException e)
				{
					e.printStackTrace();
					out.writeObject(null);
				}
			}

		}
		catch(IOException | ClassNotFoundException  e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				socket.close();
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}
		
		
	}

}
