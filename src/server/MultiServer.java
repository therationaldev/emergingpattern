package server;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
/**
* <p>Title: MultiServer</p>
* <p>Description: Classe adibita all'avvio del server.</p>
* <p>Copyright: Copyright (c) 2014</p>
* <p>Company: Metodi Avanzati di Programmazione Group</p>
* <p>Class description: Classe che modella un server in grado di accettare la richiesta trasmessa da un generico Client
* 					    e istanzia un oggetto della classe ServerOneClient.</p>
* @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Pignatelli
* @version 1.0
*/
public class MultiServer{
	
	private static final int PORT = 8080;
	/**
	 * Invoca il metodo privato {@link #run()}. 
	 */
	public MultiServer()
	{
		this.run();
	}
	/**
	 * Assegna ad una variabile locale s il riferimento ad una istanza della classe ServerSocket creata usando la porta {@value #PORT}.
	 * s si pone in attesa di richieste di connessione da parte di client in risposta alle quali viene restituito l�oggetto
	 * Socket da passare come argomento al costruttore della classe ServerOneClient.
	 */
	private void run()
	{
		ServerSocket s = null;
		
		try {
			s = new ServerSocket(PORT);
			
			while(true)
			{
				Socket socket = s.accept();
				
				try
				{
					new ServerOneClient(socket);
				}
				catch(IOException e)
				{
					e.printStackTrace();
					socket.close();
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally
		{
			try {
				s.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * Metodo main principale che avvia il server.
	 * @param args vettore di stringhe che possono contenere valori qualora caricati al momento dell'apertura del programma.
	 */
	public static void main(String[] args)
	{
		MultiServer multiServer = new MultiServer();
	}
}
