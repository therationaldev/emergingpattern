package utility;
/**
* <p>Title: Queue</p>
* <p>Description: Classe che modella una coda </p>
* <p>Copyright: Copyright (c) 2014</p>
* <p>Company: Metodi Avanzati di Programmazione Group</p>
* <p>Class description: classe modella una struttura coda che � poi usata come contenitore a modalit� FIFO per i pattern frequenti
*  scoperti a livello k da usare per generare i pattern candidati a livello k+1.</p>
* @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Idiota Pignatelli
* @version 1.0
*/
public class Queue {

		//MEMBRI ATTRIBUTI
		private Record begin = null;

		private Record end = null;
		
		//MEMBRI METODI
		/**
		 * <p>Title: Record</p>
		 * <p>Description: Classe interna che modella un record.</p>
		 * <p>Copyright: Copyright (c) 2014</p>
		 * <p>Company: Metodi Avanzati di Programmazione Group</p>
		 * <p>Class description: classe modella un record all'interno della struttura dati Queue.</p>
		 * @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Idiota Pignatelli
		 * @version 1.0
		 */
		private class Record {

	 		 Object elem;

	 		 Record next;

	 		/**
	 		  * Inizializza il record e associa l'elemento (e) passato a {@link #elem} ({@link #next} � inizializzato a null).
	 		  * @param e l'elemento che inizializza {@link #elem}.
	 		  */
			 Record(Object e) {
				this.elem = e; 
				this.next = null;
			}
		}
		
		/**
		 * Controlla se la coda � vuota.
		 * @return restituisce un booleano: true se la coda � vuota, false altrimenti.
		 */
		 public boolean isEmpty() {
			return this.begin == null;
		}

		 /**
		  * Inserisce l'elemento passato nella coda.
		  * @param e elemento da inserire nella coda.
		  */
		 public void enqueue(Object e) {
			if (this.isEmpty())
				this.begin = this.end = new Record(e);
			else {
				this.end.next = new Record(e);
				this.end = this.end.next;
			}
		}

		 /**
		  * Restituisce l'elemento in prima posizione.
		  * @return l'elemento in prima posizione.
		  */
		 public Object first(){
			return this.begin.elem;
		}

		 /**
		  * Rimuove l'elemento dalla coda, controlla inoltre se la coda � vuota e se vero stampa un messaggio.
		  */
		 public void dequeue(){
			if(this.begin==this.end){
				if(this.begin==null)
				System.out.println("The queue is empty!");
				else
					this.begin=this.end=null;
			}
			else{
				begin=begin.next;
			}
		}
}