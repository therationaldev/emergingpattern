package utility;

/**
* <p>Title: EmptyQueueException</p>
* <p>Description: Classe che modella una eccezione.</p>
* <p>Copyright: Copyright (c) 2014</p>
* <p>Company: Metodi Avanzati di Programmazione Group</p>
* <p>Class description: Classe che modella l�eccezione che occorre qualora si cerca di leggere/cancellare 
*                       da una coda � vuota.</p>
* @author Vito Vincenzo Covella, Silvia Fiorella, Fabio Pignatelli
* @version 1.0
*/
public class EmptyQueueException extends Exception {
	
	public EmptyQueueException() {}
	/**
	 * Chiama il costruttore della superclasse e modella l�eccezione che occorre qualora si cerca di leggere/cancellare da una coda � vuota.
	 * @param msg stringa che contiene il messaggio dell'eccezione.
	 */
	public EmptyQueueException(String msg)
	{
		super(msg);
	}

}
